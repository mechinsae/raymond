FROM debian:buster
RUN apt-get update && apt-get install nginx -y
RUN git clone https://github.com/aquasecurity/trivy-ci-test.git && cd trivy-ci-test
